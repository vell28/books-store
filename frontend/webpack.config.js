const path           = require('path');
const webpack        = require('webpack');
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

const main = {
  devtool: 'source-map',
  performance: {
    hints: false
  },
  mode: 'production',
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, '../public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [{
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          query: {
            presets: ["env", "react", "stage-0", "stage-1", "stage-2", "stage-3", "es2017"],
            plugins: ["transform-async-to-generator"]
          }
        }]
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [{
            loader: 'file-loader',
            options: {
              name: 'images/[name][hash].[ext]',
              outputPath: 'images/'
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              outputPath: 'images/',
              mozjpeg: {
                progressive: true,
                quality: 70
              },
              pngquant: {
                quality: '70',
                speed: 4
              }
            }
          }
        ]
      }
    ]
  },
  optimization: {
    minimizer: [
      new UglifyJSPlugin({
        uglifyOptions: {
          beautify: false,
          comments: false,
          compress: {
            sequences:    true,
            booleans:     true,
            loops:        true,
            unused:       true,
            warnings:     false,
            drop_console: true,
            unsafe:       true
          }
        }
      })
    ],
  },
};

const devConfig = {
  mode: 'development',
  devServer: {
    historyApiFallback: {
      disableDotRule: true
    },
    stats: 'errors-only',
    port: 8080
  }
};


module.exports = function (env) {
  if (env === 'production') {
    return main;
  }
  if (env === 'development') {
    return Object.assign({},
      main,
      devConfig
    )
  }
}