import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import rootReducer from './src/reducers';
import Routes from './src/routes';
import './src/style.css';

const store = createStore(rootReducer, composeWithDevTools(
    applyMiddleware(thunk)
))

ReactDOM.render(
    <Provider store={ store }>
        <Routes/>
    </Provider>,
    document.getElementById('root')
);