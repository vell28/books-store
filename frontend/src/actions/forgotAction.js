import axios from 'axios';

import {
    FORGOT_PASSWORD_SUCCES,
    FORGOR_PASSWORD_FAILUR,
} from '../const';
import { DataConfig } from '../config';

export function getUserEmail(data) {
    return dispatch => {
        return axios.post(`${DataConfig.forgot}`, {email: data})
    }
}

export function getUserEmailSuccess(data) {
    return {
        type: FORGOT_PASSWORD_SUCCES,
        data
    }
}

export function getUserEmailFailur() {
    return {
        type: FORGOR_PASSWORD_FAILUR
    }
}