import axios from 'axios';

import { 
    GET_SINGLE_BOOK
} from '../const';

import { DataConfig } from '../config';

export function getSingleBook(bookId, data) {
    return dispatch => {
        return axios.get(`${DataConfig.getSingleBook}/${bookId}`, data)
    }
}

export function getSingleBookSuccess(book) {
    return {
        type: GET_SINGLE_BOOK,
        book
    }
}
