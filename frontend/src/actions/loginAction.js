import axios from 'axios';

import {
    SUCCESS_LOGIN,
    FAILUR_LOGIN
} from '../const';
import { DataConfig } from '../config';

export function setAuthorizationToken(token) {
    if(token) {
        axios.defaults.headers.common['Authorization'] = `${token}`;
    }
}

export function Login(data) {
    return dispatch => {
        return axios.post(`${DataConfig.login}`, data)
    }
}

export function LoginSuccess(data) {
    return {
        type: SUCCESS_LOGIN,
        data
    }
}

export function LoginFailur() {
    return {
        type: FAILUR_LOGIN
    }
}