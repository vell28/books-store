import axios from 'axios';

import { 
    START_DELETE_BOOK,
    DELETE_BOOK_SUCCESS,
    DELETE_BOOK_FAILUR
} from '../const';

import { DataConfig } from '../config';

export function deleteBook(bookId, isAdmin) {
    return dispatch => {
        return axios.delete(`${DataConfig.getSingleBook}/${bookId}`, isAdmin)
    }
}

export function startDeleteBook() {
    return {
        type: START_DELETE_BOOK
    }
}

export function deleteBookSuccsess(book) {
    return {
        type: DELETE_BOOK_SUCCESS,
        book
    }
}

export function deleteBookFailur() {
    return {
        type: DELETE_BOOK_FAILUR
    }
}