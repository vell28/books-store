import axios from 'axios';

import {
    SUCCESS_REGISTRATION,
    FAILUR_REGISTRATION
} from '../const';

import { DataConfig } from '../config';

export function setRegistration(data) {
    return dispatch => {
        return axios.post(`${DataConfig.registration}`, data)
    }
}

export function setRegistrationSucc() {
    return {
        type: SUCCESS_REGISTRATION
    }
}

export function setRegistrationFeilur() {
    return {
        type: FAILUR_REGISTRATION
    }
}
