import axios from 'axios';

import {
    RESET_PASSWORD_SUCCES,
    RESET_PASSWORD_FAILUR
} from '../const';
import { DataConfig } from '../config';

export function resetPassword(data) {
    return dispatch => {
        return axios.post(`${DataConfig.resetPassword}`, data)
    }
}

export function resetPasswordSuccess(data) {
    return {
        type: RESET_PASSWORD_SUCCES,
        data
    }
}

export function resetPasswordFailur() {
    return {
        type: RESET_PASSWORD_FAILUR
    }
}