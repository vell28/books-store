import axios from 'axios';

import {
    START_FILTER_ITEMS,
    SUCCESS_FILTER_ITEMS,
    FAILUR_FILTER_ITEMS
} from '../const';

import { DataConfig } from '../config';

export function itemsFilterRequest(value){
    return dispatch => {
        return axios.post(`${DataConfig.getBooks}/filter`, value)
    }    
}

export function startFilterItems(){
    return {
        type: START_FILTER_ITEMS
    }
}

export function filterItemsSuccess(data){
    return {
        type: SUCCESS_FILTER_ITEMS,
        data
    }
}

export function failurFilterItems(error){
    return {
        type: FAILUR_FILTER_ITEMS,
        error
    }
}
