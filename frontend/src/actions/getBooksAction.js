import axios from 'axios';

import { 
    SUCCESS_LOAD_BOOKS,
    FAILUR_LOAD_BOOKS
} from '../const';
import { DataConfig } from '../config';

export function getBooks() {
    return dispatch => {
        return axios.get(`${DataConfig.getBooks}`)
    }
}

export function getBooksSuccess(data) {
    return  {
        type: SUCCESS_LOAD_BOOKS,
        data
    }
}

export function getBooksFailur() {
    return  {
        type: FAILUR_LOAD_BOOKS
    }
}