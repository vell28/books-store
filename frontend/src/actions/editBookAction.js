import axios from 'axios';

import { 
    EDIT_BOOK_SUCCESS,
    EDIT_BOOK_FAILUR
} from '../const';
import { DataConfig } from '../config';

export function editBookStart(book) {
    return dispatch => {
        return axios.put(`${DataConfig.getSingleBook}/${book._id}`, book)
    }
}

export function editBooksSuccess(data) {
    return  {
        type: EDIT_BOOK_SUCCESS,
        data
    }
}

export function editBooksFailur() {
    return  {
        type: EDIT_BOOK_FAILUR
    }
}