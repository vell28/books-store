import axios from 'axios';

import { 
    START_ADD_BOOK,
    ADD_BOOK_SUCCESS,
    ADD_BOOK_FAILUR
} from '../const';

import { DataConfig } from '../config';

export function addBooksStart(book) {
    return dispatch => {
        return axios.post(`${DataConfig.getSingleBook}`, book)
    }
}

export function startAddBook(){
    return {
        type: START_ADD_BOOK
    }
}

export function addBookSuccsess(book) {
    return {
        type: ADD_BOOK_SUCCESS,
        book
    }
}

export function addBookFailur() {
    return {
        type: ADD_BOOK_FAILUR
    }
}