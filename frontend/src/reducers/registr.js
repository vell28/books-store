import {
    SUCCESS_REGISTRATION,
    FAILUR_REGISTRATION
} from '../const';

const initialState = {}

export default function regUser(state = initialState, action) {
    switch (action.type) {
        case SUCCESS_REGISTRATION:
            return {
                ...state,
            }
        case FAILUR_REGISTRATION:
            return {
                ...state,
                fail: 'Такой логин уже существует'
            }
        default:
            return state;
    }
}