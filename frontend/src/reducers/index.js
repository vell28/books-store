import { combineReducers } from "redux";

import regUser from './registr';
import loginUser from './login';
import resetPassword from './resetPassword';
import Books from './books';

const rootReducer = combineReducers({
    regUser,
    loginUser,
    resetPassword,
    Books
});

export default rootReducer;