import {
    SUCCESS_LOAD_BOOKS,
    FAILUR_LOAD_BOOKS,
    START_FILTER_ITEMS,
    SUCCESS_FILTER_ITEMS,
    FAILUR_FILTER_ITEMS,
    START_ADD_BOOK,
    ADD_BOOK_SUCCESS,
    ADD_BOOK_FAILUR,
    START_DELETE_BOOK,
    DELETE_BOOK_SUCCESS,
    DELETE_BOOK_FAILUR,
    EDIT_BOOK_SUCCESS,
    EDIT_BOOK_FAILUR
} from '../const';

const initialState = {
    books: [],
    search: true,
    info: ''
}

export default function Books(state = initialState, action) {
    switch (action.type) {
        case SUCCESS_LOAD_BOOKS:
            return {
                ...state,
                books: action.data,
                search: false
            }
        case FAILUR_LOAD_BOOKS:
            return {
                ...state,
                search: false
            }
        case START_FILTER_ITEMS:
            return {
                ...state,
                search: true,
                info: "Производится поиск по Вашему запросу"
            };
        case SUCCESS_FILTER_ITEMS:
            return {
                ...state,
                books: action.data.data,
                search: false,
                info: ""
            };
        case FAILUR_FILTER_ITEMS:
            return {
                ...state,
                search: false,
                info: action.error
            };
        case START_ADD_BOOK:
            return {
                ...state,
                search: true
            };
        case ADD_BOOK_SUCCESS:
            return {
                ...state,
                books: [action.book, ...state.books],
                search: false
            };
        case ADD_BOOK_FAILUR:
            return {
                ...state,
            }
            START_DELETE_BOOK
        case START_DELETE_BOOK:
            return {
                ...state,
                search: true
            }     
        case DELETE_BOOK_SUCCESS:
            return {
                ...state,
                books: state.books.filter(item => {
                    return item._id !== action.book
                }),
                search: false
            }
        case DELETE_BOOK_FAILUR:
            return {
                ...state,
                search: false
            }
        case EDIT_BOOK_SUCCESS:
            return {
                ...state,
                books: state.books.map((item, index) => {
                    if (item._id === action.data._id ) {
                        return action.data;
                    }  
                    return item;
                })
            }    
        case EDIT_BOOK_FAILUR:
            return {
                ...state
            }    
        default:
            return state;
    }
}