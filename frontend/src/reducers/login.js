import {
    SUCCESS_LOGIN,
    FAILUR_LOGIN,
    LOGOUT
} from '../const';

const initialState = {
    user: {},
    login: false
};

export default function loginUser (state = initialState, action) {
    switch(action.type){
        case SUCCESS_LOGIN:
            return {
                ...state,
                user: action.data,
                login: true
            }
        case FAILUR_LOGIN:
            return {
                ...state
            }          
        case LOGOUT: 
            return {
                ...state,
                user: '',
                login: false
            }          
        default:
            return state;    
    }
}