import {
 FORGOT_PASSWORD_SUCCES,
    FORGOR_PASSWORD_FAILUR,   
    RESET_PASSWORD_SUCCES,
    RESET_PASSWORD_FAILUR
} from '../const';

const initialState = {
    userEmail: ''
}

export default function resetPassword(state = initialState, action) {
    switch (action.type) {
        case FORGOT_PASSWORD_SUCCES:
            return {
                ...state,
                userEmail: action.data
            }
        case FORGOR_PASSWORD_FAILUR:
            return {
                ...state
            }
        default:
            return state;
    }
}