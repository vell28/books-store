export const DataConfig = {
    registration: 'http://localhost:3000/api/signup',
    login: 'http://localhost:3000/api/signin',
    forgot: 'http://localhost:3000/api/reset',
    resetPassword: 'http://localhost:3000/api/reset_password',
    getBooks: 'http://localhost:3000/api/books',
    getSingleBook: 'http://localhost:3000/api/book'
}