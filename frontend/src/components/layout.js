import React from 'react';

import HeaderConteiner from '../containers/header';
import Footer from './footer';

const Layout = ({ children }) => (
    <div className='body-wrap'>
        <HeaderConteiner />
        <div className="content-wrap">
            <div className="container">
                {children}
            </div>
        </div>   
        <Footer />
    </div>
)

export default Layout;