import React from 'react'
import { withRouter } from 'react-router-dom';
import jwt from 'jsonwebtoken';

import TextFieldGroup from './formgroup';

class ForgotPassword extends React.Component {
    state = {
        email: '',
        error: ''
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault(); 
        this.setState({ error: ''}) 
        this.props.getUserEmail(this.state.email).then(
            res => {
                const token = res.data;
                const Useremail = jwt.decode(token);
                this.props.getUserEmailSuccess(Useremail.email);
                this.props.history.push(`/signin/${token}`)
            },
            rej => {
                this.setState({ error: rej.response.data.error })
                this.props.getUserEmailFailur();
            }
        )
    }
    render() {
        const { error, email } = this.state;
        return (
            <div className="row reg-wrap">
                <form
                    className="col-md-4 col-md-offset-4"
                    onSubmit={this.onSubmit.bind(this)}
                >
                    <TextFieldGroup
                        error={error}
                        type="email"
                        label="Введите Ваш eamil"
                        onChange={this.onChange.bind(this)}
                        value={this.state.email}
                        field="email"
                    />
                    <div className="form-group">
                        <input
                            type="submit"
                            className="form-control btn-primary"
                            value="send"
                            disabled={!email.length}
                        />
                    </div>
                </form>
            </div>
        )
    }
}

export default withRouter(ForgotPassword)