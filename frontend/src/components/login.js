import React from 'react'
import jwt from 'jsonwebtoken';
import { withRouter } from 'react-router-dom';

import TextFieldGroup from './formgroup';
import {
    setAuthorizationToken
} from '../actions/loginAction';

class LoginForm extends React.Component {
    state = {
        login: '',
        password: '',
        fail: '',
        info: '',
        errorLogin: '',
        errorPassword: ''
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onValidate(data) {
        this.setState({ errorLogin: '', errorPassword: '' })
        if (data.login.length < 5) {
            data.errorLogin = 'This field is required and must contain at least 5 characters';
            this.setState({errorLogin: 'This field is required and must contain at least 5 characters'})
        }
        if (data.password.length < 5) {
            data.errorPassword = 'This field is required and must contain at least 5 characters';
            this.setState({errorPassword: 'This field is required and must contain at least 5 characters'})
        }
    }

    onSubmit(e) {
        e.preventDefault();
        this.onValidate(this.state);
        const { login, password, errorLogin, errorPassword } = this.state;
        let data = {
            login,
            password
        }
        if ( !errorLogin && !errorPassword ) {
            this.props.Login( data )
                .then( res => {
                    const token = res.data.token;
                    localStorage.setItem( 'jwtToken', token );
                    setAuthorizationToken( token );
                    const data = jwt.decode( token );
                    this.props.LoginSuccess( jwt.decode( token ) );
                    this.setState({ info: 'Добро пожаловать' })

                    setTimeout(() => {
                        this.setState({ info: '' })
                        this.props.history.push('/books')
                    }, 1500)
                })
                .catch(error => {
                    this.props.LoginFailur();
                    this.setState({ fail: error.response.data.message })

                    setTimeout(() => {
                        this.setState({ fail: '' })
                    }, 1500)
                })

            setTimeout(() => {
                this.setState({ login: '', password: '' })
            })
        }
    }
    render() {
        const { 
            errorLogin, 
            errorPassword, 
            login, 
            password, 
            info, 
            fail 
        } = this.state;

        const { user } = this.props;
        
        return (
            <div className="row reg-wrap">
                {
                    info && <div className="alert alert-success">{ info }<b> { user.login }</b></div>
                }
                {
                    fail && <div className="alert alert-danger">{ fail } </div>
                }
                <form
                    className="col-md-4 col-md-offset-4"
                    onSubmit={this.onSubmit.bind(this)}
                >
                    <TextFieldGroup
                        error={errorLogin}
                        type="text"
                        label="Login"
                        onChange={this.onChange}
                        value={login}
                        field="login"
                    />
                    <TextFieldGroup
                        error={errorPassword}
                        type="password"
                        label="Password"
                        onChange={this.onChange}
                        value={password}
                        field="password"
                    />
                    <div className="form-group">
                        <input
                            type="submit"
                            className="form-control btn-primary"
                            value="send"
                            disabled={!login.length || !password.length}
                        />
                    </div>
                </form>
            </div>
        )
    }
}

export default withRouter(LoginForm)