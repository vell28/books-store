import React from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

import TextFieldGroup from '../formgroup';

class SingleBook extends React.Component {

    state = {
        _id: '',
        author: '',
        nameBook: '',
        genre: '',
        imageUrl: '',
        edit: false,
        error: '',
        info: ''
    }

    componentWillMount(){
        if (!localStorage['jwtToken']) {
            this.props.history.push('/')
        }
        const { 
            params 
        } = this.props;
        this.props.getSingleBook(params).then(
            res => {
                this.setState({ 
                    _id: res.data._id,
                    author: res.data.author,
                    nameBook: res.data.nameBook,
                    genre: res.data.genre,
                    imageUrl: res.data.imageUrl,
                });
            },
            rej => {
                this.setState({error: 'Такой Страници не существует'});
            }
        )
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, edit: true });
    }

    onSubmit(e) {
        e.preventDefault();
        const {
            _id,
            author,
            nameBook,
            genre,
            imageUrl 
        } = this.state;

        const { params } = this.props;   

        this.props.editBookStart({ _id, author, nameBook, genre, imageUrl }).then(
            res => {
                this.setState({info: 'Книга успешно отредактирована'});
                this.props.editBooksSuccess(res.data);
                setTimeout( () => {
                    this.setState({info: ''});
                }, 1500);
            },
            rej => {
                this.setState({error: 'Что-то пошло не так! попробуйте еще раз!'});
                this.props.editBooksFailur()
                setTimeout( () => {
                    this.setState({error: ''});
                }, 1500);
            }
        )
    }
    render() {

        const { 
            author, 
            nameBook, 
            genre, 
            book,
            imageUrl,
            edit,
            error,
            info 
        } = this.state;

        const { isAdmin } = this.props;

        return (
            <div className="single-page-wrap row">
                {
                    error && <div className="alert alert-danger">{error}</div>
                }
                {
                    info && <div className="alert alert-success">{info}</div>
                }
                <div className="book-wrap col-md-4">
                    <div className="cart-item-img thumbnail">
                        <img
                            src={ imageUrl }
                            alt={ nameBook }
                            className="card-img-top"
                        />
                    </div>
                </div>
                { isAdmin ?
                    <div className="form-wrap col-md-8">
                        <form 
                            className="form-inline" 
                            onSubmit={this.onSubmit.bind(this)}
                        >
                            <div className=" filter-name-wrap">
                                <TextFieldGroup
                                    type="text"
                                    label="Book name"
                                    onChange={ this.onChange }
                                    value={ nameBook }
                                    field="nameBook"
                                    className="form-control"
                                />
                                <TextFieldGroup
                                    type="text"
                                    label="Author"
                                    onChange={ this.onChange }
                                    value={ author }
                                    field="author"
                                    className="form-control"
                                />
                                <TextFieldGroup
                                    type="text"
                                    label="Genre"
                                    onChange={ this.onChange }
                                    value={ genre }
                                    field="genre"
                                    className="form-control"
                                />
                            </div>
                            <div className="form-group col-md-3">
                                <input type="submit" 
                                    className="btn btn-primary" 
                                    disabled = { !edit }
                                    value="Редактировать" 
                                />
                            </div>
                            <div className="clearfix"></div>
                        </form>
                    </div>
                        :
                    <div className="col-md-8">
                        <h3>Name: <b>{ nameBook }</b></h3>
                        <h4 className="card-title">
                            Author: <b>{ author }</b>
                        </h4>
                        <p className="card-text">
                            genre: <b>{ genre }</b>
                        </p>
                    </div>
                }
                <Link
                    to={'/books'}
                    className="btn btn-primary"
                >
                    Вернуться назад
                </Link>
            </div>
        )
    }
}

export default withRouter(SingleBook);