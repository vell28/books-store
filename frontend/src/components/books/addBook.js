import React from 'react'

import TextFieldGroup from '../formgroup';
import { setTimeout } from 'timers';

class AddBook extends React.Component {

    state = {
        genre: '',
        author: '',
        nameBook: '',
        imageUrl: "../../img/book.jpg",
        error: '',
        info: ''
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onClick = (e) => {
        document.getElementById('popup-window').style.top = -999 + 'px';
        setTimeout(() => {
            this.setState({ genre: '', author: '', nameBook: '' })
        })
    }

    onSubmit(e) {
        e.preventDefault();
        const {
            genre,
            author,
            nameBook,
            imageUrl
        } = this.state;

        this.props.startAddBook();
        this.props.addBooksStart({ genre, author, nameBook, imageUrl }).then(
            res => {
                setTimeout( () => {
                    this.setState({info: 'Книга была успешно созданна'})
                    this.props.addBookSuccsess(res.data)
                }, 1000)
                setTimeout( ()=> {
                    this.setState({info: ''})
                }, 3000)
            },
            rej => {
                setTimeout( () => {
                    this.setState({error: rej.response.data.message})
                    this.props.addBookFailur()
                }, 1500)
                setTimeout( ()=> {
                    this.setState({error: ''})
                }, 3000)
            }
        )
    }

    render() {

        const { 
            genre, 
            author, 
            nameBook 
        } = this.state;
        const { 
            error, 
            info 
        } = this.state;

        return (
            <div>
                <div className="add-book-info">
                    {
                        error && <div className="alert alert-danger" rol="alert">{error}</div>
                    }
                    {
                        info && <div className="alert alert-success" rol="info">{info}</div>
                    }
                </div>
                 <div className="drop-popup-wrap" id="popup-window">
                    <div className="popup-wrap">
                        <span
                            onClick={this.onClick}
                            className="glyphicon glyphicon-remove"
                        >
                        </span>
                        <form onSubmit={this.onSubmit.bind(this)}>
                            <TextFieldGroup
                                type="text"
                                label="Add Book Name"
                                onChange={ this.onChange }
                                value={ nameBook }
                                field="nameBook"
                            />
                            <TextFieldGroup
                                type="text"
                                label="Add Author"
                                onChange={ this.onChange }
                                value={ author }
                                field="author"
                            />
                            <TextFieldGroup
                                type="text"
                                label="Add Genre"
                                onChange={ this.onChange }
                                value={ genre }
                                field="genre"
                            />
                            <div className="form-group">
                                <input
                                    type="submit"
                                    className="form-control btn-primary"
                                    value="send"
                                    disabled={!genre || !author || !nameBook}
                                    onClick={this.onClick}
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddBook;