import React from 'react'
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { setTimeout } from 'timers';

class AllBooks extends React.Component {

    state = {
        error: '',
        info: ''
    }

    componentWillMount() {

        if (!localStorage['jwtToken']) {
            this.props.history.push('/');
        } else {
            const {books} = this.props;
            if(!books.length){
                this.props.getBooks().then(
                    res => {
                        setTimeout(() => {
                            this.props.getBooksSuccess(res.data.books);
                        }, 1200)
                    },
                    rej => {
                        this.setState({error: 'К сожалению книг нет'});
                        setTimeout(() => {
                            this.props.getBooksFailur();
                            this.setState({error: ''});
                        }, 2500)
                    }
                )
            }
        }
    }

    onDelete = (e) => {
        const data_id = e.target.getAttribute('data-id');
        this.props.startDeleteBook();
        this.props.deleteBook(data_id).then(
            res => {

                setTimeout(() => {
                    this.setState({info: res.data.message});
                    this.props.deleteBookSuccsess(data_id);
                },1500)

                setTimeout(() => {
                    this.setState({info: ''});
                }, 3000);

            },
            rej => {

                setTimeout(() => {
                    this.setState({error: rej.response});
                    this.props.deleteBookFailur();
                },1500)

                setTimeout(() => {
                    this.setState({error: ''});
                }, 2500);
            }    
        )
    }

    render() {
        const { books, search, isAdmin } = this.props;
        const {error, info} = this.state;
        return (
            <div className="books-wrap">
                {
                    error && <div className="alert alert-danger" rol="alert">{error}</div>
                }
                {
                    info && <div className="alert alert-success" rol="info">{info}</div>
                }
                {!search ?
                    books.map((item, i) => {
                        return <div key={i} className="card item-wrap col-md-3 col-sm-4" >
                            <h3>Name: <b>{item.nameBook}</b></h3>
                            <div className="cart-item-img thumbnail">
                                <img
                                    src={item.imageUrl}
                                    alt={item.nameBook}
                                    className="card-img-top"
                                />
                            </div>
                            <h4 className="card-title">
                                Author: <b>{item.author}</b>
                            </h4>
                            <p className="card-text">genre: <b>{item.genre}</b></p>
                            <Link
                                to={`/book/${item._id}`}
                                className="btn btn-primary"
                            >
                                Узнать подробней
                            </Link>
                            { isAdmin && 
                                <div>
                                    <Link
                                        to={`/book/${item._id}`}
                                        className="btn btn-primary"
                                    >
                                        Редактировать книгу
                                    </Link>
                                    <button 
                                        className="btn btn-danger" 
                                        onClick={this.onDelete}
                                        data-id={item._id}  
                                    >
                                        Удалить
                                    </button>
                                </div>
                            }    
                        </div>
                    })
                    :
                    <div className="spinner-wrap">
                        <img src="../img/spinner.gif" alt="spinner" />
                    </div>
                }
            </div>
        )
    }
}

export default withRouter(AllBooks);