import React from 'react';

import TextFieldGroup from '../formgroup';

class FilterBar extends React.Component {
    state = {
        inputFilter: ''
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();
        this.props.startFilterItems();
        this.props.itemsFilterRequest({ val: this.state.inputFilter }).then(
            res => {
                setTimeout( () => {
                    this.props.filterItemsSuccess( res.data )
                }, 1000)
            },
            rej => {
                setTimeout( () => {
                    this.props.failurFilterItems(rej.response.data.message)
                }, 1000)
            }
        )   
    }
    render() {
        const { info } = this.props;
        return (
            <div className="form-wrap row">
                <form className="form-inline" onSubmit={this.onSubmit.bind(this)}>
                    <div className="col-md-10 filter-name-wrap">
                        <TextFieldGroup
                            type="text"
                            onChange={this.onChange.bind(this)}
                            value={this.state.inputFilter}
                            field="inputFilter"
                            className="form-control"
                            placeholder="Поиск"
                        />
                    </div>
                    <div className="form-group col-md-2">
                        <input type="submit" 
                            className="btn btn-primary" 
                            value="Search" 
                        />
                    </div>
                    <div className="clearfix"></div>
                </form>
                {info && <div className="alert alert-info" role="alert">{info}</div>}
            </div>
        )
    }
}

export default FilterBar;