import React from 'react';
import { NavLink } from 'react-router-dom';
import jwt from 'jsonwebtoken';
import { withRouter } from 'react-router-dom';

import { setAuthorizationToken } from '../actions/loginAction';

class HeaderComponent extends React.Component {

    componentWillMount() {
        if (localStorage['jwtToken']) {
            const user = jwt.decode(localStorage['jwtToken']);
            this.props.LoginSuccess(user);
            setAuthorizationToken(localStorage['jwtToken']);
        }
    }

    Logout = () => {
        localStorage.removeItem('jwtToken');
        this.props.logoutAction();
    }

    render() {

        const { 
            login,
            user 
        } = this.props;

        return (
            <div>
                <nav className="header">
                    <div className="container ">
                        <NavLink className="col-md-2" to={"/"}>
                            <img src="./img/logo.png" alt="logo" />
                        </NavLink>
                        <div className="col-md-9 header-link-wrap">
                            { !login ?
                                <ul className="header-link">
                                    <li>
                                        <NavLink 
                                            activeClassName={ "active" } 
                                            exact={ true } 
                                            to={ '/signin' }
                                        >
                                            Signin
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink 
                                            activeClassName={ "active" } 
                                            exact={ true } 
                                            to={ '/signup' }
                                        >
                                            Signup
                                        </NavLink>
                                    </li>
                                </ul>
                                :
                                <ul className="header-link">    
                                    <li>
                                        <NavLink 
                                            activeClassName={ "active" } 
                                            exact={ true } 
                                            to={ '/books' }
                                        >
                                            Books
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink 
                                            activeClassName={ "active" } 
                                            exact={ true } 
                                            to={'/'}
                                            onClick={ this.Logout }
                                        >
                                            Logout
                                        </NavLink>
                                    </li>   
                                </ul>     
                            }
                        </div>
                        <div className="col-md-1">
                            {
                                login && <div><b>{ user.login }</b></div>
                            }
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

export default withRouter(HeaderComponent);
