import React from 'react';

const TextFieldGroup = ({ field, value, label, error, type, onChange, placeholder }) => {
    return(
        <div className="form-group">
            <label className="control-label">{label}</label>
            <input 
                value = {value}
                onChange = {onChange}
                type={type}
                name={field}
                className="form-control"
                placeholder={placeholder}
            />
            {error && <span className="text-danger">{error}</span>}
        </div>
    )
}

export default TextFieldGroup;