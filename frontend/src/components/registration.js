import React from 'react'
import { withRouter } from 'react-router-dom';

import TextFieldGroup from '../components/formgroup';

class RegistrationForm extends React.Component {
    state = {
        login: '',
        email: '',
        password: '',
        reg_status: false,
        error: ''
    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onValidate(data) {
        if (data.login.length < 5) {
            data.errorLogin = 'This field is required and must contain at least 6 characters';
            this.setState({})
        }
        if (!data.email.length) {
            data.errorEmail = 'This field is required and must be valide email';
            this.setState({})
        }
        if (data.password.length < 5) {
            data.errorPassword = 'This field is required and must contain at least 6 characters';
            this.setState({})
        }
    }

    onSubmit(e) {
        this.setState({ fail: '' })
        e.preventDefault();
        this.onValidate(this.state)
        const { login, email, password, errorLogin, errorEmail, errorPassword } = this.state;
        const data = {
            login,
            email,
            password
        }
        this.props.setRegistration(data)
        .then(res => {
                this.setState({ login: '', email: '', password: '' })
                this.props.setRegistrationSucc();
                this.setState({ reg_status: true })
                setTimeout(() => {
                    this.props.history.push('/')
                }, 1500);
            })
        .catch(error => {
                this.props.setRegistrationFeilur();
                this.setState({ fail: error.response.data.error })
            })
    }

    render() {
        
        const { 
            login, 
            email, 
            password, 
            errorLogin, 
            errorEmail, 
            errorPassword, 
            reg_status, 
            fail 
        } = this.state;

        return (
            <div className="row reg-wrap ">
                <h1>Registration Form</h1>
                {
                    reg_status && <div className="alert alert-success">Регистрация прошла успешно</div>
                }
                {
                    fail && <div className="alert alert-danger">{ fail }</div>
                }
                {!reg_status && 
                    <form
                        className="col-md-4 col-md-offset-4"
                        onSubmit={this.onSubmit.bind(this)}
                    >
                        <TextFieldGroup
                            error={errorLogin}
                            type="text"
                            label="Login"
                            onChange={this.onChange.bind(this)}
                            value={login}
                            field="login"
                        />
                        {
                            fail && <div className="alert alert-warnin ">{ fail }</div>
                        }
                        <TextFieldGroup
                            error={errorEmail}
                            type="email"
                            label="Email"
                            onChange={this.onChange.bind(this)}
                            value={email}
                            field="email"
                        />
                        <TextFieldGroup
                            error={errorPassword}
                            type="password"
                            label="Password"
                            onChange={this.onChange.bind(this)}
                            value={password}
                            field="password"
                        />
                        <div className="form-group">
                            <input
                                type="submit"
                                className="form-control btn-primary"
                                value="send"
                                disabled={!login.length || !email.length || !password.length}
                            />
                        </div>
                    </form>
                }
            </div>
        )
    }
}

export default withRouter(RegistrationForm)