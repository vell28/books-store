import React from 'react'
import { withRouter } from 'react-router-dom';
import jwt from 'jsonwebtoken';

import TextFieldGroup from './formgroup';
import { setTimeout } from 'timers';

class ResetPassword extends React.Component {

    state = {
        password: '',
        passwordCconfirm: '',
        pasError: '',
        confirmPasError: '',
        info: ''
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onValidate(data) {
        this.setState({ pasError: '', confirmPasError: '' })
        if (data.password.length < 5) {
            data.pasError = 'Паротль должен состоять не мение чем с 5 символов';
            this.setState({ pasError: 'Паротль должен состоять не мение чем с 5 символов' })
        }
        if (data.passwordCconfirm !== data.password) {
            data.confirmPasError = 'Пароль должен соответствовать';
            this.setState({ confirmPasError: 'Пароль должен соответствовать' })
        }
    }

    onSubmit(e) {
        e.preventDefault();
        this.onValidate(this.state)

        const {
            pasError,
            confirmPasError,
            password
        } = this.state;

        const { userEmail } = this.props;

        if (!pasError && !confirmPasError) {
            this.props.resetPassword({ password: password, email: userEmail }).then(
                res => {
                    this.props.resetPasswordSuccess(res.data);
                    this.setState({ info: 'Пароль успешно изменен' })
                    setTimeout(() => {
                        this.setState({ info: '' })
                        this.props.history.push('/home')
                    }, 1500)

                },
                rej => {
                    this.setState({ error: rej.response.data.error })
                    this.props.resetPasswordFailur();
                    setTimeout(() => {
                        this.setState({ error: '' })
                    }, 1500)
                }
            )
        } else {
            return;
        }
    }

    render() {
        
        const {
            error,
            info,
            password,
            passwordCconfirm,
            pasError,
            confirmPasError
        } = this.state;

        return (
            <div className="row reg-wrap">
                {
                    info && <div className="alert alert-success">{info}</div>
                }
                {
                    error && <div className="alert alert-danger">{error} </div>
                }
                <form
                    className="col-md-4 col-md-offset-4"
                    onSubmit={this.onSubmit.bind(this)}
                >
                    <TextFieldGroup
                        error={pasError}
                        type="password"
                        label="Введите новый пароль"
                        onChange={this.onChange}
                        value={password}
                        field="password"
                    />
                    <TextFieldGroup
                        error={confirmPasError}
                        type="password"
                        label="Повторите пароль"
                        onChange={this.onChange}
                        value={passwordCconfirm}
                        field="passwordCconfirm"
                    />
                    <div className="form-group">
                        <input
                            type="submit"
                            className="form-control btn-primary"
                            value="send"
                            disabled={!password.length || !passwordCconfirm.length}
                        />
                    </div>
                </form>
            </div>
        )
    }
}

export default withRouter(ResetPassword)