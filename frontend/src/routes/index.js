import React from 'react';
import { Router, Route, BrowserRouter } from 'react-router-dom';

import Layout from '../components/layout';
import Home from '../components/home';
import loginPage from '../containers/loginPage';
import Registration from '../containers/registration';
import ResetPaswordPage from '../containers/resetPassword';
import BooksPage from '../containers/booksPage';
import BookSinglePage from '../containers/bookSinglePage';

class Routes extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Layout>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/signin' component={loginPage} />
                    <Route exact path='/signup' component={Registration} />
                    <Route exact path='/signin/:token' component={ResetPaswordPage} />
                    <Route exact path='/books' component={BooksPage} />
                    <Route exact path='/book/:id' component={BookSinglePage} />
                </Layout>
            </BrowserRouter>
        )
    }
}

export default Routes;