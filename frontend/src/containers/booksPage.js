import React from 'react';
import { connect } from 'react-redux';

import FilterBar from '../components/books/filterBooks';
import AllBooks from '../components/books/allBooks';
import AddBook from '../components/books/addBook';

import { 
    getBooks,
    getBooksSuccess,
    getBooksFailur
} from '../actions/getBooksAction';

import {
    itemsFilterRequest, 
    startFilterItems,
    filterItemsSuccess,
    failurFilterItems
} from '../actions/filterBooksAction';

import { 
    startAddBook,
    addBooksStart,
    addBookSuccsess,
    addBookFailur
} from '../actions/addBookAction';

import{
    startDeleteBook,
    deleteBook,
    deleteBookSuccsess,
    deleteBookFailur
} from '../actions/deleteBookAction';

class BooksPage extends React.Component {

    onClick = () => {
        document.getElementById('popup-window').style.top = 0 + 'px';
    }

    render(){
        const { 
            getBooks,
            getBooksSuccess,
            getBooksFailur, 
            startFilterItems,
            filterItemsSuccess, 
            failurFilterItems, 
            itemsFilterRequest,
            startAddBook,
            addBooksStart, 
            addBookSuccsess, 
            addBookFailur,
            startDeleteBook,
            deleteBook,
            deleteBookSuccsess,
            deleteBookFailur 
        } = this.props;

        const { 
            books, 
            search, 
            info 
        } = this.props.getAllBooks;

        const { user } = this.props.loginUser;

        return(
            <div>
                <FilterBar 
                    startFilterItems = {startFilterItems}
                    itemsFilterRequest = {itemsFilterRequest}
                    filterItemsSuccess = {filterItemsSuccess}
                    failurFilterItems = {failurFilterItems}
                    info = {info}
                    books = {books}
                />
                { user.isAdmin && 
                    <button 
                        className = "btn btn-success add-book"
                        onClick = { this.onClick }   
                    >  
                        <b>+ Add Book</b>
                    </button>  
                }     
                <AllBooks
                    getBooks = {getBooks}
                    getBooksSuccess = {getBooksSuccess}
                    getBooksFailur = {getBooksFailur}
                    startDeleteBook = {startDeleteBook}
                    deleteBook = {deleteBook}
                    deleteBookSuccsess = {deleteBookSuccsess}
                    deleteBookFailur = {deleteBookFailur}
                    books = {books}
                    search = {search}
                    isAdmin = {user.isAdmin}
                /> 
                <AddBook
                    startAddBook ={ startAddBook}
                    addBooksStart = {addBooksStart}
                    addBookSuccsess = {addBookSuccsess}
                    addBookFailur = {addBookFailur}
                />
            </div>   
        )
    }
}

const mapDispatchToProps = {
    getBooks,
    getBooksSuccess,
    getBooksFailur,
    startFilterItems,
    itemsFilterRequest,
    filterItemsSuccess,
    failurFilterItems,
    startAddBook,
    addBooksStart,
    addBookSuccsess,
    addBookFailur,
    startDeleteBook,
    deleteBook,
    deleteBookSuccsess,
    deleteBookFailur
}

const mapStateToProps = (state) => {
    return {
        getAllBooks: state.Books,
        loginUser: state.loginUser
    }   
}

export default connect(mapStateToProps, mapDispatchToProps)(BooksPage);