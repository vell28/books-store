import React from 'react';
import { connect } from 'react-redux';

import HeaderComponent from '../components/header';
import { LoginSuccess } from '../actions/loginAction';
import { logoutAction } from '../actions/logoutAction';

class HeaderConteiner extends React.Component {

    render() {

        const {  
            logoutAction,
            LoginSuccess
        } = this.props;
        const { 
            login,
            user  
        } = this.props.user;

        return (
            <div>
                <HeaderComponent
                    LoginSuccess = {LoginSuccess}
                    logoutAction = {logoutAction}
                    login = {login}
                    user = {user}
                />
            </div>
        )
    }
}

const mapDispatchToProps = {
    logoutAction,
    LoginSuccess
}

const mapStateToProps = (state) => {
    return {
        user: state.loginUser
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderConteiner);
