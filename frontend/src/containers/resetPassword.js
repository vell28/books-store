import React from 'react';
import { connect } from 'react-redux';

import ResetPassword from '../components/resetPassword';

import { 
    resetPassword,
    resetPasswordSuccess,
    resetPasswordFailur
} from '../actions/resetPasAction';

class ResetPaswordPage extends React.Component {

    render(){

        const { 
            resetPassword, 
            resetPasswordSuccess, 
            resetPasswordFailur 
        } = this.props;
        const { userEmail } = this.props.userEmail;
        return(
            <div>
                <ResetPassword
                    resetPassword={resetPassword}
                    resetPasswordSuccess={resetPasswordSuccess}
                    resetPasswordFailur={resetPasswordFailur}
                    userEmail={userEmail}
                /> 
            </div>   
        )
    }
}

const mapDispatchToProps = {
    resetPassword,
    resetPasswordSuccess,
    resetPasswordFailur
}

const mapStateToProps = (state) => {
    return {
        userEmail: state.resetPassword
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPaswordPage);