import React from 'react';
import { connect } from 'react-redux';

import LoginForm from '../components/login';
import ForgotPassword from '../components/forgotPassword';

import {
    Login,
    LoginSuccess,
    LoginFailur
} from '../actions/loginAction';
import { 
    getUserEmail,
    getUserEmailSuccess,
    getUserEmailFailur
} from '../actions/forgotAction';

class loginPage extends React.Component {
    
    state = {
        showLoginForm: true
    }

    onClick = () => {
        this.setState({showLoginForm: false})
    }

    render(){

        const { 
            Login, 
            LoginSuccess, 
            LoginFailur, 
            getUserEmail, 
            getUserEmailSuccess, 
            getUserEmailFailur 
        } = this.props;
        const { user } = this.props.loginUser;
        const { showLoginForm } = this.state;
        
        return(
            <div>
                { showLoginForm ? 
                    <LoginForm 
                        Login={Login}
                        LoginSuccess={LoginSuccess}
                        LoginFailur={LoginFailur}
                        user={user}
                    />
                    :
                    <ForgotPassword
                        getUserEmail={getUserEmail}
                        getUserEmailSuccess={getUserEmailSuccess}
                        getUserEmailFailur={getUserEmailFailur}
                    /> }
                <br/>
                { showLoginForm &&
                    <div
                        className="btn btn-primary col-md-2 col-md-offset-5"
                        onClick={this.onClick}
                    >
                        Forgot Password
                    </div>
                }
            </div>   
        )
    }
}

const mapDispatchToProps = {
    Login,
    LoginSuccess,
    LoginFailur,
    getUserEmail,
    getUserEmailSuccess,
    getUserEmailFailur
}

const mapStateToProps = (state) => {
    return {
        loginUser: state.loginUser
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(loginPage);