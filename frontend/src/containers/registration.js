import React from 'react';

import RegistrationForm from '../components/registration';
import { connect } from 'react-redux';

import {
    setRegistration,
    setRegistrationSucc,
    setRegistrationFeilur
} from '../actions/registrationAction';

class Registration extends React.Component {

    render() {

        const { 
            setRegistration, 
            setRegistrationSucc, 
            setRegistrationFeilur 
        } = this.props;
        const { fail } = this.props.regUser;
        
        return (
            <div>
                <RegistrationForm
                    setRegistration={setRegistration}
                    setRegistrationSucc={setRegistrationSucc}
                    setRegistrationFeilur={setRegistrationFeilur}
                    fail={fail}
                />
            </div>
        )
    }
}

const mapDispatchToProps = {
    setRegistration,
    setRegistrationSucc,
    setRegistrationFeilur
}

const mapStateToProps = (state) => {
    return {
        regUser: state.regUser
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
