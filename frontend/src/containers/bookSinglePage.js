import React from 'react';

import SingleBook from '../components/books/singleBook';
import { connect } from 'react-redux';

import {
    getSingleBook,
    getSingleBookSuccess
} from '../actions/getSingleBookAction';

import {
    editBookStart,
    editBooksSuccess,
    editBooksFailur
} from '../actions/editBookAction';

class BookSinglePage extends React.Component {

    render() {

        const { 
            getSingleBook, 
            getSingleBookSuccess,
            editBookStart,
            editBooksSuccess,
            editBooksFailur
        } = this.props;

        const { books } = this.props.book;
        const { isAdmin } = this.props.admin.user;
        const params = this.props.match.params.id;
        
        return (
            <div>
                <SingleBook
                    getSingleBook = {getSingleBook}
                    getSingleBookSuccess = {getSingleBookSuccess}
                    editBookStart = {editBookStart}
                    editBooksSuccess = {editBooksSuccess}
                    editBooksFailur = {editBooksFailur}
                    books = {books}
                    isAdmin = {isAdmin}
                    params = {params}
                />
            </div>
        )
    }
}

const mapDispatchToProps = {
    getSingleBook,
    getSingleBookSuccess,
    editBookStart,
    editBooksSuccess,
    editBooksFailur
}

const mapStateToProps = (state) => {
    return {
        book: state.Books,
        admin: state.loginUser
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookSinglePage);
