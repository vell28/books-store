import mongoose, {Schema} from 'mongoose';
import bcrypt from 'bcrypt-as-promised';

const UserShema = new Schema({
    login: { type: String, unique: true, lowercase: true, index: true, required: true},
    email: { type: String, unique: true, lowercase: true, required: true},
    password: {type: String, required: true},
    isAdmin: {type: Boolean, default: false}
});

UserShema.pre('save', async function(next) {
    if(!this.isModified('password')) {
        return next();
    }

    if( this.login === 'admin' && this.password) {
        this.isAdmin = true;
    }

    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(this.password, salt);

    this.password = hash;
    next();

})

UserShema.methods.comparePasswords = function(password) {
    return bcrypt.compare(password, this.password);
}

export default mongoose.model('Users', UserShema);