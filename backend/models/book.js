import mongoose, { Schema } from 'mongoose';
import config from '../config/database';

const BookSchema = new Schema({
    nameBook: { type: String, require: true, lowercase: true},
    imageUrl: { type: String, require: true, lowercase: true},
    author: { type: String, require: true, lowercase: true },
    genre : { type: String, require: true, lowercase: true }
})


export default mongoose.model( 'Books', BookSchema );