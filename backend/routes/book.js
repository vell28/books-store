import express from 'express';

import * as BookController from '../controllers/book';

const router = express.Router();

router.post('/book', BookController.create);
router.get('/books', BookController.getAll);
router.post('/books/filter', BookController.getFilterBooks);
router.get('/book/:id', BookController.getSingleBook);
router.put('/book/:id', BookController.edit);
router.delete('/book/:id', BookController.deleteBook);

export default router;