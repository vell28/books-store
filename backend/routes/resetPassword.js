import express from 'express';

import * as Reset from '../controllers/resetPassword';

const router = express.Router();

router.post('/reset_password', Reset.resetPassword);

export default router;