import express from 'express';

import * as forgotPassword from '../controllers/forgot';

const router = express.Router();

router.post('/reset', forgotPassword.getForgotEmail);

export default router;
