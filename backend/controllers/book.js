import Books from '../models/book';
import User from '../models/user';

export async function create(req, res, next) {

    let book = req.body;

    try {
        book = await Books.create(book);
    } catch ({ message }) {
        return next({
            status: 400,
            message
        })
    }

    res.json(book);
}

export async function getFilterBooks(req, res, next) {
    let books,
        data;
    const { val } = req.body;
    try {
        books = await Books.find({});
        data = await books.filter(item => {
            return item.genre.includes(val.toLowerCase()) ||
                item.author.includes(val.toLowerCase()) ||
                item.nameBook.includes(val.toLowerCase())
        })

        if (!data.length) {
            throw new Error(['ничего не найдено по вашему запросу']);
        }

    } catch ({ message }) {
        return next({
            status: 400,
            message
        })
    }

    res.json({ data });
}

export async function getAll(req, res, next) {
    let books;
    try {
        books = await Books.find({});
    } catch ({ message }) {
        return next({
            status: 400,
            message
        })
    }

    res.json({ books });
}

export async function getSingleBook(req, res, next) {
    let book;
    try {
        book = await Books.findOne({ _id: req.params.id });
    } catch ({ message }) {
        return next({
            status: 400,
            message
        })
    }

    res.json(book);
}

export async function edit(req, res, next) {
    let book;

    const {
        _id,
        author,
        nameBook,
        genre,
        imageUrl
    } = req.body

    try {
        book = await Books.findOne({ _id: _id }, function (err, book) {
            book.genre = genre;
            book.author = author;
            book.nameBook = nameBook;

            book.save(function (err) {
                if (err) {
                    console.error('ERROR!');
                }
                return res.json( book );    
            });
        });

    } catch ({ message }) {
        return next({
            status: 400,
            message
        });
    }
}

export async function deleteBook(req, res, next) {
    const _id = req.params.id;
    let book;
    try {
        book = await Books.findOne({ _id });
    } catch ({ message }) {
        return next({
            status: 400,
            message
        })
    }

    if (!book) {
        return next({
            status: 404,
            message: 'Page not found'
        })
    }

    try {
        book.remove();
    } catch ({ message }) {
        return next({
            status: 400,
            message
        })
    }

    return res.json({ message: 'Книга была удалена' });
}
