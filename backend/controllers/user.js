import * as UserService from '../services/userService';

export async function getCurrentUser(req, res, next) {
    const { token } = req;
    let user;

    try {
        user = await UserService.getUserByToken(token);  
        
    } catch ({error}) {
      
        return next({
            status: 500,
            error: 'token is not valid'
        })
    }

    return res.status(200).json(user);
}
