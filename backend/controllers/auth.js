import jwt from 'jsonwebtoken';

import User from '../models/user';
import config from '../config/database';

export const signup = async (req, res, next) => {

    let user;
    const { login, password } = req.body;
    try {
        user = await User.create(req.body);    
    } catch (error) {
        error = {error:'This login is already taken'};
        return res.status(400).json(error);
    }

    res.status(200).json(user);
}   

export const signin = async (req, res, next) => {
    const { login, password } = req.body;

    const user = await User.findOne({ login });

    if(!user) {
        return next({
            status: 400,
            message: 'Вы ввели неверный пароль или логин1'
        })
    }

    try {
        const result = await user.comparePasswords(password);
    } catch (e) {
        return next({
            status: 400,
            message: 'Вы ввели неверный пароль'
        })
    }
    
    const token = jwt.sign(
        {
            _id: user._id, 
            login: user.login, 
            isAdmin: user.isAdmin 
        },
        config.secret);
        
    res.json({token});

}    