import User from '../models/user';

export const resetPassword = async (req, res, next) => {

    const { email, password } = req.body;
    try{
        let user = await User.findOne({ email: email}, function(err, user){
            user.password = password;
            user.save(function (err) {
                if(err) {
                    console.error('ERROR!');
                }
            });
        })
        let message;
        res.status(200).json({message: 'Пароль был успешно заменен'});
    } catch ({error}) {
        error = {error:'This login is already taken'};
        return res.status(400).json(error);
    }
}       