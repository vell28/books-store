import jwt from 'jsonwebtoken';

import config from '../config/database';
import User from '../models/user';

export const getForgotEmail = async (req, res, next) => {

    const { email } = req.body;
    let userEmail;
    userEmail = await User.findOne({ email: email});
    
    if(userEmail) {
        const userResetToken = jwt.sign({_id: userEmail._id, email: userEmail.email }, config.secret);
        res.status(200).json(`${userResetToken}`);
    } else if (!userEmail) {
        const error = {error:'This email not faund'};
        return res.status(400).json(error);
    }
 
} 