import express from 'express';
import mongoose from 'mongoose';
import session from 'express-session';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import bluebird from 'bluebird';
import cors from 'cors';

import config from './config/database';
import authRoute from './routes/auth';
import userRoute from './routes/user';
import forgotPassword from './routes/forgot';
import resetPassword from './routes/resetPassword';
import bookRoutes from './routes/book';

import errorHendler from './middlewares/errorHandler';
import getUser from './middlewares/getUser';
import checkToken from './middlewares/checkToken';

const app = express();

mongoose.Promise = bluebird;
mongoose.connect(config.database, err => {
    if(err){
        throw err;
    }
    console.log('mongoDB connected')
})

app.listen(config.port, err => {
    if(err) throw err;

    console.log(`Server listening on port ${config.port}`);
})

app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session ({
    resave: true,
    saveUninitialized: true,
    secret: config.secret
}))

app.use(express.static(__dirname + '/../public'))
app.use('/books', express.static(__dirname + '/../public'))
app.use('/book/:id', express.static(__dirname + '/../public'))
app.use('/singup', express.static(__dirname + '/../public'))
app.use('/signin', express.static(__dirname + '/../public'))

app.use(cors());
app.use('/api', authRoute);
app.use('/api', forgotPassword);
app.use('/api', resetPassword);
app.use('/api', checkToken, userRoute);
app.use(getUser);
app.use('/api', checkToken, bookRoutes);

app.use(errorHendler);